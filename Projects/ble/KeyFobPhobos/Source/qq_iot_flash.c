/*********************************************************************
 * INCLUDES
 */
#include <string.h>
#include "bcomdef.h"
#include "OnBoard.h"
#include "hal_flash.h"
#include "osal_snv.h"
#include "qq_iot_flash.h"
/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */


/*********************************************************************
 * LOCAL FUNCTIONS
 */

/* For din store...
 * One variable one page! This makes things easy.
 */
#if 1
static uint8 g_activePage;
static uint16 g_CurOffset;
#define STORE_PAGE_BEGIN_DIN  85 //Should be different to STORE_PAGE_BEGIN
#define STORE_PAGE_END_DIN   STORE_PAGE_BEGIN_DIN
#define OFFSET_END_DIN       HAL_FLASH_PAGE_SIZE
#define STEP_COUNT_DIN       8
uint8 qq_iot_snv_din_init(void)
{
    g_activePage = STORE_PAGE_BEGIN_DIN;
    g_CurOffset = 0;
    uint8 pg;
    uint16 offset = 0;
    uint32 ret[2];
    uint32 nothingsFF[2] = {0xFFFFFFFF,0xFFFFFFFF};
    bool ifGetOne = FALSE;
    for (pg = STORE_PAGE_BEGIN_DIN; pg <= STORE_PAGE_END_DIN; pg++)
    {
        for (offset = 0; offset < OFFSET_END_DIN; offset+=STEP_COUNT_DIN)
        {
            HalFlashRead(pg, offset, (uint8*)&ret, STEP_COUNT_DIN);
            if (memcmp( ret, nothingsFF, STEP_COUNT_DIN ) == 0)
            {
                g_activePage = pg;
                g_CurOffset = offset;
                ifGetOne = TRUE;
                break;
            }
        }
        if (ifGetOne == TRUE)
        {
            break;
        }
    }
    if (ifGetOne == FALSE)
    {
        uint8 page = STORE_PAGE_END_DIN;
        uint16 offset = OFFSET_END_DIN - STEP_COUNT_DIN;
        uint32 rets[1];
        HalFlashRead(page, offset, (uint8 *)rets, STEP_COUNT_DIN);
        #if 1
        uint8 i;
        for (i = STORE_PAGE_BEGIN_DIN; i <= STORE_PAGE_END_DIN; i++)
        {
            HalFlashErase(i);//Flash-page erase time: 20 ms
        }
        #endif
        uint16 addr = (g_CurOffset >> 2) + ((uint16)g_activePage << 9);
        HalFlashWrite(addr, (uint8 *)rets, 2);
        g_CurOffset = STEP_COUNT_DIN;
    }
    else
    {
    }
    return 0;
}

/*
 * pBuf length must be 8 bytes
 */
uint8 qq_iot_snv_din_read( void *pBuf )
{
    uint8 ret[STEP_COUNT_DIN];
    uint16 realOffset = g_CurOffset;
    uint8 realActivePg = g_activePage;
    if (g_CurOffset == 0)
    {
        if (g_activePage == STORE_PAGE_BEGIN_DIN)
        {
            HalFlashRead(g_activePage, g_CurOffset, ret, STEP_COUNT_DIN);
        }
        else
        {
            realOffset = OFFSET_END_DIN - STEP_COUNT_DIN;
            realActivePg--;
            HalFlashRead(realActivePg, realOffset, ret, STEP_COUNT_DIN);
        }
    }
    else
    {
        realOffset -= STEP_COUNT_DIN;
        HalFlashRead(g_activePage, realOffset, ret, STEP_COUNT_DIN);
    }
    uint32 nothingsFF[2] = {0xFFFFFFFF,0xFFFFFFFF};
    if (memcmp(ret, nothingsFF, STEP_COUNT_DIN) == 0)
    {
        return NV_OPER_FAILED;
    }
    memcpy(pBuf, ret, STEP_COUNT_DIN);

    return SUCCESS;
}

/*
 * pBuf length must be 8 bytes
 */
uint8 qq_iot_snv_din_write( void *pBuf )
{
    uint16 realOffset = g_CurOffset;
    if (realOffset >= OFFSET_END_DIN)
    {
        HalFlashErase(STORE_PAGE_BEGIN_DIN);
        realOffset = 0;
    }
    uint16 addr = (realOffset >> 2) + ((uint16)g_activePage << 9);
    HalFlashWrite(addr, pBuf, 2);

    g_CurOffset = realOffset + STEP_COUNT_DIN;
    return 0;
}
#endif
